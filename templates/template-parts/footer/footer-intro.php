<?php

	$footer_intro = get_field('footer_intro', 'option');
	$image = $footer_intro['image'];
	$copy = $footer_intro['copy'];

?>

	<div class="container py-4">
		<div class="row flexible-content">
			<div class="grid-2-column order-md-initial col-12 col-md-6 order-1">
				<img class="img-full" src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>">
			</div>
			<div class="grid-2-column order-md-initial col-12 col-md-6 order-2">
				<?php echo $copy; ?>
			</div>
		</div>
	</div>