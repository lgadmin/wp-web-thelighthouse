<div class="utility-bar d-none d-md-block">
  <div class="container">
    <div class="left">
        <div>
          <a class="text-white mr-0 mr-md-4" href="mailto:<?php echo do_shortcode('[lg-email]'); ?>"><i class="fa fa-envelope mt-1 mr-1" aria-hidden="true"></i> <span><?php echo do_shortcode('[lg-email]'); ?></span></a>
        </div>
        <div>
          <a class="text-white" href="tel:<?php echo do_shortcode('[lg-phone-main]'); ?>"><i class="fa fa-phone mt-1 mr-1" aria-hidden="true"></i> <span>Call us today: <?php echo format_phone(do_shortcode('[lg-phone-main]')); ?></span></a>
        </div>
    </div>
    <div class="right">
        <a class="text-white py-3 px-1 bg-secondary" href="<?php echo wc_get_cart_url(); ?>"><i class="fas fa-shopping-cart"></i> <?php echo WC()->cart->get_cart_contents_count(); ?> items</a>
    </div>
  </div>
</div>