<?php
get_header(); ?>

	<div id="primary">
		<div id="content" role="main" class="site-content">
			<main>

				<?php get_template_part( '/components/acf-flexible-layout/layouts' ); ?>
				
			</main>
		</div>
	</div>

<?php get_footer(); ?>