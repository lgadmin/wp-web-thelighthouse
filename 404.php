<?php
/**
 * Template for displaying 404 pages (Not Found)
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
 */

get_header(); ?>

	<div id="primary">
		<div id="content" role="main">

			<div class="py-5 my-5 text-center">
				<h1 class="h2">Page not found: 404</h1>
				<p>Looks like the page you're searching for is not exist. Please double check if you entered correct url or contact us at <a href="mailto:<?php echo do_shortcode('[lg-email]'); ?>"><?php echo do_shortcode('[lg-email]'); ?></a></p>
			</div>

		</div><!-- #content -->
	</div><!-- #primary -->

<?php get_footer(); ?>