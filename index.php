<?php
/**
 * Main template file
 *
 */
?>

<?php get_header(); ?>

	<div id="primary">
		<main id="content" role="main" class="site-content py-4">
			<div class="body-copy py-2">
				
				<div class="blog-banner">
					<?php
						$banner = get_field('blog_default_banner', 'option');
					?>
					<img src="<?php echo $banner['url']; ?>" alt="<?php echo $banner['alt']; ?>">
				</div>

				<?php get_template_part( 'templates/template-parts/content/content-loop'); ?>

			</div>
			<?php get_sidebar(); ?>
		</main>
	</div>

<?php get_footer(); ?>