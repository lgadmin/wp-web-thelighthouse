<?php 
/**
 * Grid List Layout
 *
 */
?>

<?php

	get_template_part('/components/acf-flexible-layout/partials/block-settings-start');

?>

<!--------------------------------------------------------------------------------------------------------------------------------->

<?php

	// Block Fields
	$block_title = get_sub_field('block_title');
	$items = get_sub_field('grid_list');
	$item_per_row = get_sub_field('item_per_row');
	$grid_item_class = get_sub_field('grid_item_class');

?>

<?php if($items && is_array($items)): ?>
	<?php foreach ($items as $key => $value): ?>
		<div class="col col-12 col-sm-6 col-md-<?php echo 12 / $item_per_row; ?> d-flex flex-column <?php echo $grid_item_class; ?>"><?php echo $value['content']; ?></div>
	<?php endforeach; ?>
<?php endif; ?>

<!--------------------------------------------------------------------------------------------------------------------------------->

<?php 

	get_template_part('/components/acf-flexible-layout/partials/block-settings-end');

?>