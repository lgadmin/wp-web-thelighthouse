<?php 
/**
 * Text Block Layout
 *
 */
?>

<?php

	get_template_part('/components/acf-flexible-layout/partials/block-settings-start');

?>

<!--------------------------------------------------------------------------------------------------------------------------------->

	<div class="col-12 woocommerce">
		
		<?php
			$categories = get_sub_field('categories');
			$category_ids = [];

			if($categories && is_array($categories)){
				foreach ($categories as $key => $cat) {
					array_push($category_ids, $cat->term_id);
				}
			}

		   $args = array(
			    'post_type'             => 'product',
			    'post_status'           => 'publish',
			    'showposts'        		=> -1,
			    'tax_query'             => array(
			        array(
			            'taxonomy'      => 'product_cat',
			            'field' 		=> 'term_id', //This is optional, as it defaults to 'term_id'
			            'terms'         => $category_ids,
			            'include_children'	=> false,
			            'operator'      => 'AND' // Possible values are 'IN', 'NOT IN', 'AND'.
			        )
			    )
			);

		   $result = new WP_Query( $args );

			// Loop
		    if ( $result->have_posts() ) :
		    	?>
		    	
				<div class="product-slider products">
			    	<?php
			        while( $result->have_posts() ) : $result->the_post(); 
			        global $woocommerce;
			    	?>
			    	
			        <div class="single-product-slider">
			        	<?php wc_get_template_part('content', 'product'); ?>
			        </div>

					<?php
			        endwhile;
			        ?>
		        </div>
		    <?php else: ?>
		    <p class="center pt-xs">No product found</p>
		        <?php

		    endif; // End Loop

		    wp_reset_query();
		?>

	</div>

<!--------------------------------------------------------------------------------------------------------------------------------->

<?php 

	get_template_part('/components/acf-flexible-layout/partials/block-settings-end');

?>
