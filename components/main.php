<?php

function lg_component_enqueue_styles_scripts() {

    wp_enqueue_style( 'lg-component-style', get_stylesheet_directory_uri() . '/components/assets/css/components.min.css', [], filemtime(get_template_directory() . '/components/assets/css/components.min.css') );
	
}
add_action( 'wp_enqueue_scripts', 'lg_component_enqueue_styles_scripts' );

?>