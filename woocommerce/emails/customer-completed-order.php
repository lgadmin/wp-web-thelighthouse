<?php
/**
 * Customer completed order email
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/customer-completed-order.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates/Emails
 * @version     2.5.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * @hooked WC_Emails::email_header() Output the email header
 */
do_action( 'woocommerce_email_header', $email_heading, $email ); ?>

<?php
	if ( class_exists( 'WC_Checkout_Field_Editor' ) ) {
		$order_id = version_compare( WC_VERSION, '3.0', '<' ) ? $order->id : $order->get_id();
		$shipping = get_post_meta( $order_id, 'order_shipping', true );
		$order_number = $order->get_order_number();

		if($shipping == 'Pick up'){
			echo "<p>The status of an order you recently placed on our website has changed.</p>
				<p>The current status of order #($order_number) is now <b>Ready for pick up</b> at:</p>
				<p>9789 92a Ave<br>
				Langley, BC<br>
				V1M 3B3, Canada</p>
				<p>For office hours and more information, please call our office <a href='tel:604-371-2211'>604-371-2211</a></p>";
		}else{
			echo "<p>The status of an order you recently placed on our website has changed.</p>
				<p>The current status of order #($order_number) is now <b>Shipped</b></p>
				<h3>Shipment Tracking Numbers / Links</h3>
				<p>Please contact a member of our team for shipping details at 604-371-2211</p>
				";
		}

		echo "<br>";
	}
?>

<?php

/**
 * @hooked WC_Emails::order_details() Shows the order details table.
 * @hooked WC_Structured_Data::generate_order_data() Generates structured data.
 * @hooked WC_Structured_Data::output_structured_data() Outputs structured data.
 * @since 2.5.0
 */
do_action( 'woocommerce_email_order_details', $order, $sent_to_admin, $plain_text, $email );

/**
 * @hooked WC_Emails::order_meta() Shows order meta data.
 */
do_action( 'woocommerce_email_order_meta', $order, $sent_to_admin, $plain_text, $email );

/**
 * @hooked WC_Emails::customer_details() Shows customer details
 * @hooked WC_Emails::email_address() Shows email address
 */
do_action( 'woocommerce_email_customer_details', $order, $sent_to_admin, $plain_text, $email );

/**
 * @hooked WC_Emails::email_footer() Output the email footer
 */
do_action( 'woocommerce_email_footer', $email );
