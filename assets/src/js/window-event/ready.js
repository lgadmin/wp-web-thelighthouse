// Windows Ready Handler

(function($) {

    $(document).ready(function(){
      	//Mobile Toggle
        $('.mobile-toggle').on('click', function(){
      		$('.navbar-collapse').fadeToggle();
      	});

        $('.dropdown-toggle').on('mouseover', function(e){
          $(this).siblings('.dropdown-menu').addClass('show');
        });

        $('.dropdown-toggle').on('mouseleave', function(e){
          $(this).siblings('.dropdown-menu').removeClass('show');
        });

        $('.feature-slider').slick({
          autoplay: true,
          fade: true,
          arrows: false,
          dots: false,
          autoplaySpeed: 6000,
          adaptiveHeight: true
        });

        $('.product-slider').slick({
          autoplay: true,
          arrows: true,
          dots: false,
          autoplaySpeed: 6000,
          slidesToShow: 4,
          slidesToScroll: 1,
          infinite: true,
          responsive: [
            {
              breakpoint: 1024,
              settings: {
                autoplay: true,
                arrows: true,
                dots: false,
                autoplaySpeed: 6000,
                slidesToShow: 3,
                slidesToScroll: 1,
                infinite: true
              }
            },
            {
              breakpoint: 768,
              settings: {
                autoplay: true,
                arrows: true,
                dots: false,
                autoplaySpeed: 6000,
                slidesToShow: 2,
                slidesToScroll: 1,
                infinite: true
              }
            },
            {
              breakpoint: 480,
              settings: {
                autoplay: true,
                arrows: true,
                dots: false,
                autoplaySpeed: 6000,
                slidesToShow: 1,
                slidesToScroll: 1,
                infinite: true
              }
            }
          ]
        });

        //Mega Menu
        if($('.mega-menu-toggle')[0]){
          $('.mobile-toggle').on('click', function(){
            $('.mega-menu-toggle').click();
          });
        }

        $('.product_button').mouseover(function(){
          $(this).addClass('hover');
        });

        $('.product_button').mouseleave(function(){
          $(this).removeClass('hover');
        });

        $('select.orderby').selectric();

        //Woocommerce

        if($('.single-product-top')[0]){
          var quantity = $('.input-text.qty');
          $('.quantity_field .value').append(quantity);
        }
        
    });

}(jQuery));