<?php

	global $lg_style;
	
	$lg_style = array(
	    'title' => 'Style',
	    'items' =>  array(
	    	array(
				'title' => 'Heading Decorator',
	            'selector' => 'h1,h2,h3,h4,h5,h6,.h1,.h2,.h3,.h4,.h5,.h6',
	            'classes' => 'heading-decorator'
			)
	    )
	)

?>