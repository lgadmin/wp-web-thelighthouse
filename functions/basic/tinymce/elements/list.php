<?php

	global $lg_tinymce_list;

	$lg_tinymce_list = array(
	    'title' => 'List',
	    'items' =>  array(
			array(
				'title' => 'Unstyled',
				'selector' => 'ul, ol',
				'classes'	=> 'list-unstyled'
			)
	    )
	)

?>