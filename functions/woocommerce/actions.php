<?php

	add_action('add_meta_boxes', 'remove_short_description', 999);

	add_action( 'woocommerce_before_shop_loop_item_title', 'lg_woocommerce_template_loop_product_thumbnail_before', 9 );
	add_action( 'woocommerce_before_shop_loop_item_title', 'lg_woocommerce_template_loop_product_button_before', 11 );
	add_action( 'woocommerce_before_shop_loop_item_title', 'lg_woocommerce_template_loop_view_product', 12 );
	add_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_add_to_cart', 13 );
	add_action( 'woocommerce_before_shop_loop_item_title', 'lg_woocommerce_template_loop_product_button_after', 14 );
	add_action( 'woocommerce_before_shop_loop_item_title', 'lg_woocommerce_template_loop_product_thumbnail_after', 15 );

	add_action( 'woocommerce_after_single_product_summary', 'lg_woocommerce_single_product_details', 19 );

	//add_action( 'woocommerce_single_product_summary', 'lg_woocommerce_sku', 5 );
	add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_rating', 11 );
	add_action( 'woocommerce_single_product_summary', 'lg_woocommerce_product_short_description', 12 );
	
	add_action('woocommerce_add_to_cart_redirect', 'resolve_dupes_add_to_cart_redirect');
	add_action('save_post', 'lg_assign_parent_terms', 10, 2);
	add_action('lg_product_category_nav', 'lg_product_category_nav');

	add_action('woocommerce_after_cart', 'woocommerce_cart_disclaimer');

	remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10 );
	remove_action( 'woocommerce_before_shop_loop_item', 'woocommerce_template_loop_product_link_open', 10 );

	remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_rating', 10 );
	remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );

	remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10 );

	function lg_assign_parent_terms($post_id, $post){

	    if($post->post_type != 'product')
	        return $post_id;

	    // get all assigned terms   
	    $terms = wp_get_post_terms($post_id, 'product_cat' );
	    foreach($terms as $term){
	        while($term->parent != 0 && !has_term( $term->parent, 'product_cat', $post )){
	            // move upward until we get to 0 level terms
	            wp_set_post_terms($post_id, array($term->parent), 'product_cat', true);
	            $term = get_term($term->parent, 'product_cat');
	        }
	    }
	}

	function lg_product_category_nav(){
		$args = [
			'taxonomy'	=> 'product_cat',
			'show_count'	=> true,
			'hide_empty'	=> false,
			'exclude'		=> '16',
			'title_li'		=> ''
		];
		echo '<div class="text-dark-gray font-weight-bold h4  mt-4 mb-0">SHOP</div>';
		echo '<ul class="product-cat-list py-3 list-unstyled">';
		wp_list_categories($args);
		echo '</ul>';
	}

	function lg_woocommerce_sku(){
		global $product;

		echo '<div class="sku">PRODUCT NO: ' . $product->get_sku() . '</div>';
	}

	function lg_woocommerce_template_loop_product_thumbnail_before(){
		echo '<div class="lg-woo-product-thumbnail">';
	}

	function lg_woocommerce_template_loop_product_thumbnail_after(){
		echo '</div>';
	}

	function lg_woocommerce_template_loop_product_button_before(){
		echo '<div class="product_button">';
	}

	function lg_woocommerce_template_loop_product_button_after(){
		echo '</div>';
	}

	function lg_woocommerce_template_loop_view_product(){
		global $product;
		echo "<a class='view-product' href='" . get_permalink($product->get_id()) . "'>VIEW ITEM</a>";
	}

	function lg_woocommerce_single_product_details(){
		get_template_part( '/templates/template-parts/page/single-product-details' );
	}

	function remove_short_description() {
		remove_meta_box( 'postexcerpt', 'product', 'normal');
	}

	function lg_woocommerce_product_short_description(){
		echo get_field('short_description');
	}

	function resolve_dupes_add_to_cart_redirect($url = false) {
	     if(!empty($url)) { return $url; }
	     return get_bloginfo('wpurl').add_query_arg(array(), remove_query_arg('add-to-cart'));
	}

	function woocommerce_cart_disclaimer(){
		$shipping_disclaimer = get_field('shipping_disclaimer', 'option');

		if(WC()->cart->get_cart_contents_count() > 0){
		ob_start();
		?>
		<hr>
		<div class="py-3 bg-gray">

			<?php echo $shipping_disclaimer; ?>
		
		</div>
		<?php
		echo ob_get_clean();
		}
	}

?>