<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package _s
 */

?>

	<?php global $post; ?>
	<?php if(!is_blog() && !is_404() && !is_cart() && !is_checkout() && $post->post_name != 'thank-you'): ?>
		<?php get_template_part('/templates/template-parts/footer/footer-intro'); ?>
	<?php endif; ?>

	<footer id="colophon" class="site-footer text-center text-md-left">
		
		<div id="site-footer" class="py-4 bg-image" style="background-image: url(<?php the_field('footer_background_image', 'option'); ?>);">
			<div class="container">
				<div class="logo">
                  <?php echo has_custom_logo() ? get_custom_logo() : do_shortcode('[lg-site-logo]'); ?>
                </div>
                <div class="footer-nav">
                	<!-- Main Menu  -->
	                <?php 

	                  $mainMenu = array(
	                    // 'menu'              => 'menu-1',
	                    'theme_location'    => 'bottom-nav',
	                    'depth'             => 2,
	                    'container'         => 'div',
	                    'container_id'      => 'footer-navbar',
	                    'menu_class'        => 'py-2'
	                  );
	                  wp_nav_menu($mainMenu);

	                ?>
                </div>
			</div>
		</div>

		<div id="site-legal">
			<div class="container py-2">
				<div class="site-info"><?php get_template_part("/templates/template-parts/footer/site-info"); ?></div>
				<div class="site-longevity"> <?php get_template_part("/templates/template-parts/footer/site-footer-longevity"); ?> </div>
			</div>
		</div>

	</footer><!-- #colophon -->

<?php wp_footer(); ?>

</body>
</html>
